#import gen_examples as g
#n=1000
#x=g.examples(n)
#xs=sorted(x, key=lambda l:l[0])
#print(xs)
#ys=sorted(x, key=lambda l:l[1])
#print(ys)


#import gen_examples as g
#n=1000
#x=g.examples(n)
def min_err(x):
	n=len(x)

	xs=sorted(x, key=lambda l:l[0])
	ys=sorted(x, key=lambda l:l[1])
	gerr=[]
	init_err=sum([x[i][3]*(x[i][2]==-1) for i in range(n)])
	#left and on is -1 right 1
	running_err = init_err
	err=[]
	for i in range(n):
		running_err += xs[i][3]*xs[i][2]
		err.append(running_err)
	minimum_err = min(err)
	argmin_err = err.index(minimum_err)
	gerr.append([minimum_err,xs[argmin_err][0]])

	#right is -1 left and on is 1
	running_err = 1-init_err
	err=[]
	for i in range (n):
		running_err -=xs[i][3]*xs[i][2]
		err.append(running_err)
	minimum_err = min(err)
	argmin_err = err.index(minimum_err)
	gerr.append([minimum_err,xs[argmin_err][0]])
	

	#upper is 1 and lower and on is -1
	running_err = init_err
	err=[]
	for i in range(n):
		running_err +=ys[i][3]*ys[i][2]
		err.append(running_err)
	minimum_err = min(err)
	argmin_err = err.index(minimum_err)
	gerr.append([minimum_err,ys[argmin_err][1]])

	
	#upper is -1 and lower and on is 1
	running_err = 1-init_err
	err=[]
	for i in range(n):
		running_err -=ys[i][3]*ys[i][2]
		err.append(running_err)
	minimum_err = min(err)
	argmin_err = err.index(minimum_err)
	gerr.append([minimum_err,ys[argmin_err][1]])

	gmin_err=1
	for i in range(4):
		if gerr[i][0]<gmin_err:
			gmin_err=gerr[i][0]
			mini = i
	#print(gmin_err)

	argminx = gerr[mini][1]
	if mini<2:
		argmin_orient = 'vertical'
	else:
		argmin_orient = 'horizontal'
	if mini%2==0: #Left or Bottom
		argmin_sign = -1 
	else:
		argmin_sign = +1
		
	return [argminx,argmin_orient,argmin_sign, gmin_err]

	
def pvalue(h, x1, x2):
	x1h=h[0]
	ori=h[1]
	if ori is "horizontal":
		if x1h>x2:
			return h[2]
		else:
			return (-1)*h[2]
	else:
		if x1h>x1:
			return h[2]
		else:
			return (-1)*h[2]

def adaboost(x, T):
	import math
	weak_hypo=[]
	alphat=[]
	
	for t in range(T):
		print('Round...',t)
		ht=min_err(x)
		print(ht)
		weak_hypo.append(ht)
		et=weak_hypo[t][3]
		alpha=.5*(math.log((1-et)/et))
		alphat.append(alpha)
		n=len(x)
		Zt=0
		for i in range(n):#map function

			x[i][3]=x[i][3]*math.exp(-alpha*x[i][2]*pvalue(ht,x[i][0],x[i][1]))
			Zt+=x[i][3]
		for i in range(n):
			x[i][3]=x[i][3]/Zt
	return weak_hypo, alphat


def H(x, wh, at):#x is a point with x posi y posi; wh is weak hypothesis and at is alpha at t
	votes = sum([at[i]*pvalue(wh[i],x[0],x[1]) for i in range(len(at))])
	if votes < 0:
		return -1
	else:
		return 1
'''
	sum=0
	T=len(at)
	for i in range(T):
		sum+=at[i]*pvalue(wh[i], x[0], x[1])
	if sum<0:
		return -1
	else:
		return 1
'''


def main():
	print("Testing Minerror_funtion")
	import gen_examples as g
	n=10000
	x=g.examples(n)
	#print(min_err(x))
	wh,at=adaboost(x, 100)
	#print(H([0,0], wh, at))

	import numpy as np
	import matplotlib.colors
	import matplotlib.pyplot as plt

	mx=[]
	my=[]
	color=[]
	npts = 100

	mx=[2*i/npts-1 for i in range(npts) for j in range(npts)]
	my=[2*j/npts-1 for i in range(npts) for j in range(npts)]
	colors=['skyblue','lightgrey']
		
	color=[colors[H([mx[i],my[i]],wh,at)==-1] for i in range(len(mx))]
	'''
	for i in range(1000):
		for j in range(1000):
			if i<j: #H([2*i/100-1, 2*j/100-1], wh, at)==1:
				color.append('skyblue')
			else:
				color.append('lightgrey')
	'''
	plt.scatter(mx, my, c=color, alpha=0.5,edgecolors='none')
	plt.show()

main()
