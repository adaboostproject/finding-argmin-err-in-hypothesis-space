# Generates n test examples using f from function*.
# The points lie in [-1,1]x[-1,1]


import random

from function1 import f

def examples(n):
	r=random.random
	posvals=[]
	for i in range(n):
		xtemp = 2*r()-1
		ytemp = 2*r()-1
		'''
		if xtemp**2+ytemp**2<0.25:
			valtemp=1
		else:
			valtemp=-1
		'''
		valtemp = f(xtemp,ytemp)
		posvals.append([xtemp,ytemp,valtemp,1/n])
	return posvals

